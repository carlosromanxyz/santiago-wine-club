<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Santiago_Wine_Club
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php    /**
			* Displays a navigation menu
			* @param array $args Arguments
			*/
			$args = array(
				'theme_location' => 'footer',
				'menu' => '',
				'container' => 'div',
				'container_class' => 'menu-{menu-slug}-container',
				'container_id' => '',
				'menu_class' => 'menu',
				'menu_id' => '',
				'echo' => true,
				'fallback_cb' => 'wp_page_menu',
				'before' => '',
				'after' => '',
				'link_before' => '',
				'link_after' => '',
				'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
				'depth' => 0,
				'walker' => ''
			);
		
			wp_nav_menu( $args ); ?>

			<div id="footer-pagos" class="text-center">
				<div class="col-xs-12 col-lg-4">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo_paypal.png" class="img-responsive" />
				</div>
				<div class="col-xs-12 col-lg-4">
					<img src="<?php bloginfo('template_directory'); ?>/images/logo_webpay.png" class="img-responsive" />
				</div>
				<div class="col-xs-12 col-lg-4" style="margin-top: 30px;">
					<p>@<?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
				</div>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
