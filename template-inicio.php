<?php
/**
 * Template name: Inicio
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Santiago_Wine_Club
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">
				<div class="col-xs-12">
					
					<div class="slider-wrapper theme-default">
						<div class="ribbon"></div>
						<div id="slider" class="nivoSlider">
							<?php
							/**
							 * The WordPress Query class.
							 * @link http://codex.wordpress.org/Function_Reference/WP_Query
							 *
							 */
							$args = array(
								//Type & Status Parameters
								'post_type'   => 'slider'
							);
							
							$query = new WP_Query( $args ); ?>
							<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
								<a href="<?php the_field('url_del_slider'); ?>">
									<?php the_post_thumbnail( 'home-slider', array('class' => 'img-responsive')); ?>
								</a>
							<?php endwhile; endif; ?>
							<?php wp_reset_postdata(); ?>
						</div>
					</div>

					<div id="home-contacto">
						<div class="col-xs-12 col-lg-4 home-widget">
							<h3><?php _e('Contacto', 'santiago-wine-club'); ?></h3>
							<ul>
								<li>
									<span><?php _e('Teléfono', 'santiago-wine-club'); ?></span>
									<!--<p><?php // the_field('telefono', 'option'); ?></p>-->
									<p>+56 2 2632 6596</p>
								</li>
								<li>
									<span><?php _e('Celular', 'santiago-wine-club'); ?></span>
									<p>+56 9 8199 3773</p>
								</li>
								<li>
									<span><?php _e('Email', 'santiago-wine-club'); ?></span>
									<p>ventas@santiagowineclub.cl</p>
								</li>
								<div class="separata"></div>
								<li>
									<span><?php _e('Horario de atencion', 'santiago-wine-club'); ?></span>
									<br>
									<strong>Lunes, Martes, Miércoles</strong>
									<p>de 11:00 a 20:30 hrs.</p>
									<strong>Jueves, Viernes y Sábados</strong>
									<p>de 11:00 a 21:00 hrs.</p>
									<strong>Domingo</strong>
									<p>de 11:00 a 19:00 hrs.</p>
								</li>
							</ul>
						</div>
						<div class="col-xs-12 col-lg-4 home-widget">
							<h3><?php _e('Síguenos en Twitter', 'santiago-wine-club'); ?></h3>
							<a class="twitter-timeline" href="https://twitter.com/stgowineclub" data-widget-id="538090402373722112">Tweets por el @stgowineclub.</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
						<div class="col-xs-12 col-lg-4 home-widget">
							<h3><?php _e('Encuentranos en Facebook', 'santiago-wine-club'); ?></h3>
							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSantiagoWineClub&tabs=timeline&width=340&height=290&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=142169582485474" height="290" style="width:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>

					<div id="home-redes" class="col-xs-12 home-widget-full text-center">
						<!--Facebook -->
						<a href="https://www.facebook.com/SantiagoWineClub">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<!-- Twitter -->
						<a href="https://twitter.com/stgowineclub">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<!-- Pinterest -->
						<a href="http://www.pinterest.com/mallexpress/santiago-wine-club/">
							<i class="fa fa-pinterest" aria-hidden="true"></i>
						</a>
						<!-- Youtube -->
						<a href="http://www.youtube.com/watch?v=uH4MT11fDWY">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a>
						<!-- Linkedin -->
						<a href="https://www.linkedin.com/vsearch/p?company=Santiago+Wine+Club&trk=prof-0-ovw-curr_pos">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a>
						<!-- Foursquare -->
						<a href="http://santiagowineclub.cl/#">
							<i class="fa fa-foursquare" aria-hidden="true"></i>
						</a>
						<!-- Tripadvisor -->
						<a href="http://www.tripadvisor.com/Attraction_Review-g294305-d5502998-Reviews-Santiago_Wine_Club_Lastarria-Santiago_Santiago_Metropolitan_Region.html">
							<i class="fa fa-tripadvisor" aria-hidden="true"></i>
						</a>
						
									
					</div>

					<div id="home-destacados">
						<?php
						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$args = array(
							//Type & Status Parameters
							'post_type'   => 'destacado'
						);
						
						$query = new WP_Query( $args ); ?>
						<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<div class="col-xs-12 col-lg-3 home-destacado">
								<a href="<?php the_field('enlace_del_articulo_destacado'); ?>">
									<?php the_post_thumbnail('home-destacado', array('class' => 'img-responsive')); ?>
									<h3><?php the_title(); ?></h3>
									<p><?php the_content(); ?></p>
								</a>
							</div>
						<?php endwhile; endif; ?>
						<?php wp_reset_postdata(); ?>
					</div>

					<div id="home-productos">
						<h2 class="col-xs-12 text-center"><?php _e('Productos destacados', 'santiago-wine-club'); ?></h2>
						<?php
						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$meta_query   = WC()->query->get_meta_query();
						$meta_query[] = array(
						    'key'   => '_featured',
						    'value' => 'yes'
						);
						$args = array(
						    'post_type'   =>  'product',
						    'showposts'   =>  4,
						    'orderby'     =>  'date',
						    'order'       =>  'DESC',
						    'meta_query'  =>  $meta_query
						);
						
						$query = new WP_Query( $args ); ?>
						<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); $product = get_product( $query->post->ID); ?>
							<div class="col-xs-12 col-lg-3 home-producto">
								<?php the_post_thumbnail( 'medium', array('class' => 'img-responsive')); ?>
								<h3><?php the_title(); ?></h3>
								<a href="<?php the_permalink(); ?>"><?php _e('Ver producto', 'santiago-wine-club'); ?></a>
							</div>
						<?php endwhile; endif; ?>
						<?php wp_reset_postdata(); ?>
						<div class="clear"></div>
					</div>

					<ul id="home-alianzas" class="col-xs-12 col-lg-6 text-center list-unstyled">
						<h2 class="text-center"><?php _e('Alianzas', 'santiago-wine-club'); ?></h2>
						<?php
						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$args = array(
							//Type & Status Parameters
							'post_type'   => 'alianza'
						);
						
						$query = new WP_Query( $args ); ?>
						<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail('home-alianzas', array('class' => 'img-responsive')); ?>
								</a>
							</li>
						<?php endwhile; endif; ?>
						<?php wp_reset_postdata(); ?>
					</ul>

					<ul id="home-agendas" class="col-xs-12 col-lg-6 text-center list-unstyled">
						<h2 class="text-center"><?php _e('Agenda Santiago Wine Club', 'santiago-wine-club'); ?></h2>
						<?php
						/**
						 * The WordPress Query class.
						 * @link http://codex.wordpress.org/Function_Reference/WP_Query
						 *
						 */
						$args = array(
							//Type & Status Parameters
							'post_type'   => 'agenda'
						);
						
						$query = new WP_Query( $args ); ?>
						<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
							<?php // get raw date
							$date = get_field('fecha_del_evento', false, false);


							// make date object
							$date = new DateTime($date); ?>
							<li class="col-xs-12 col-lg-6">
								<div class="date">
									<span class="day"><?php echo $date->format('j'); ?></span>
									<span class="month"><?php echo $date->format('M'); ?></span>
								</div>
								<div class="event_description">
									<span><?php the_title(); ?></span>
								</div>
							</li>
						<?php endwhile; endif; ?>
						<?php wp_reset_postdata(); ?>
					</ul>
				</div>	
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
