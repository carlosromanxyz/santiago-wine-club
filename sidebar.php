<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Santiago_Wine_Club
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-xs-12 col-lg-4" role="complementary">
	<?php if(is_page('sobre-el-vino')) : ?>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<?php endif; ?>

	<?php if(is_page('contactenos')) : ?>
		<section class="widget">
			<h2 class="widget-title"><?php _e('Santiago Wine Club Lastarria', 'santiago-wine-club'); ?></h2>
			<ul class="list-unstyled">
				<li>
					<p><strong><?php _e('Dirección', 'santiago-wine-club'); ?><strong></p>
					<p><?php _e('Rosal 386, local 2, Santiago Centro.', 'santiago-wine-club'); ?></p>
					<p><?php _e('Barrio Lastarria', 'santiago-wine-club'); ?></p>
				</li>
				<li>
					<p><strong><?php _e('Teléfono', 'santiago-wine-club'); ?></strong></p>
					<p>+56 2 26326596</p>
					<p><strong><?php _e('Móvil', 'santiago-wine-club'); ?></strong></p>
					<p>+56 9 81993773</p>
				</li>
				<li>
					<p><strong><?php _e('Correo electrónico', 'santiago-wine-club'); ?></strong></p>
					<p><a href="mailto:ventas@santiagowineclub.cl">ventas@santiagowineclub.cl</a></p>
				</li>
			</ul>
		</section>
	<?php else : ?>
		<section class="widget">
			<h2 class="widget-title"><?php _e('Síguenos en Twitter', 'santiago-wine-club'); ?></h2>
			<a class="twitter-timeline" href="https://twitter.com/stgowineclub" data-widget-id="538090402373722112">Tweets por el @stgowineclub.</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</section>

		<section class="widget">
			<h2 class="widget-title"><?php _e('Encuentranos en Facebook', 'santiago-wine-club'); ?></h2>
			<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FSantiagoWineClub&tabs=timeline&width=340&height=290&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=142169582485474" height="290" style="width:100%;border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
		</section>
	<?php endif; ?>
</aside><!-- #secondary -->
