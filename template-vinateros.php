<?php
/**
 * Template name: Viñateros
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Santiago_Wine_Club
 */

get_header(); ?>

	<div id="primary" class="content-area col-xs-12 col-lg-8">
		<main id="main" class="site-main" role="main">

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->

		<ul class="list-unstyled">
			<?php
			/**
			 * The WordPress Query class.
			 * @link http://codex.wordpress.org/Function_Reference/WP_Query
			 *
			 */
			$args = array(
				//Type & Status Parameters
				'post_type'   => 'vinatero'
			);
			
			$query = new WP_Query( $args ); ?>
			<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
				<li class="col-xs-12">
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>
				</li>
			<?php endwhile; endif; ?>
			<?php wp_reset_postdata(); ?>
		</ul>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
