<?php

// Register Custom Post Type
function slider_post_type() {

	$labels = array(
		'name'                  => _x( 'Sliders', 'Post Type General Name', 'santiago-wine-club' ),
		'singular_name'         => _x( 'Slider', 'Post Type Singular Name', 'santiago-wine-club' ),
		'menu_name'             => __( 'Sliders', 'santiago-wine-club' ),
		'name_admin_bar'        => __( 'Slider', 'santiago-wine-club' ),
		'archives'              => __( 'Archivos de sliders', 'santiago-wine-club' ),
		'parent_item_colon'     => __( 'Slider principal:', 'santiago-wine-club' ),
		'all_items'             => __( 'Todos los sliders', 'santiago-wine-club' ),
		'add_new_item'          => __( 'Añadir nuevo slider', 'santiago-wine-club' ),
		'add_new'               => __( 'Añadir nuevo', 'santiago-wine-club' ),
		'new_item'              => __( 'Nuevo slider', 'santiago-wine-club' ),
		'edit_item'             => __( 'Editar slider', 'santiago-wine-club' ),
		'update_item'           => __( 'Actualizar slider', 'santiago-wine-club' ),
		'view_item'             => __( 'Ver slider', 'santiago-wine-club' ),
		'search_items'          => __( 'Buscar slider', 'santiago-wine-club' ),
		'not_found'             => __( 'Nada encontrado', 'santiago-wine-club' ),
		'not_found_in_trash'    => __( 'Nada encontrado en la papelera', 'santiago-wine-club' ),
		'featured_image'        => __( 'Imagen destacada', 'santiago-wine-club' ),
		'set_featured_image'    => __( 'Seleccionar como slider', 'santiago-wine-club' ),
		'remove_featured_image' => __( 'Remover como slider', 'santiago-wine-club' ),
		'use_featured_image'    => __( 'Usar esta imagen como slider', 'santiago-wine-club' ),
		'insert_into_item'      => __( 'Insertar en el slider', 'santiago-wine-club' ),
		'uploaded_to_this_item' => __( 'Subido a este slider', 'santiago-wine-club' ),
		'items_list'            => __( 'Lista de sliders', 'santiago-wine-club' ),
		'items_list_navigation' => __( 'Navegar por la lista de sliders', 'santiago-wine-club' ),
		'filter_items_list'     => __( 'Filtrar lista de sliders', 'santiago-wine-club' ),
	);
	$args = array(
		'label'                 => __( 'Slider', 'santiago-wine-club' ),
		'description'           => __( 'Sliders', 'santiago-wine-club' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-images-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'slider', $args );

}
add_action( 'init', 'slider_post_type', 0 );

function destacado_post_type() {

	$labels = array(
		'name'                  => _x( 'Destacados', 'Post Type General Name', 'santiago-wine-club' ),
		'singular_name'         => _x( 'Destacado', 'Post Type Singular Name', 'santiago-wine-club' ),
		'menu_name'             => __( 'Destacados', 'santiago-wine-club' ),
		'name_admin_bar'        => __( 'Destacado', 'santiago-wine-club' ),
		'archives'              => __( 'Archivos de articulos destacados', 'santiago-wine-club' ),
		'parent_item_colon'     => __( 'Destacado principal:', 'santiago-wine-club' ),
		'all_items'             => __( 'Todos los destacados', 'santiago-wine-club' ),
		'add_new_item'          => __( 'Añadir destacado', 'santiago-wine-club' ),
		'add_new'               => __( 'Añadir nuevo', 'santiago-wine-club' ),
		'new_item'              => __( 'Nuevo destacado', 'santiago-wine-club' ),
		'edit_item'             => __( 'Editar destacado', 'santiago-wine-club' ),
		'update_item'           => __( 'Actualizar destacado', 'santiago-wine-club' ),
		'view_item'             => __( 'Ver destacado', 'santiago-wine-club' ),
		'search_items'          => __( 'Buscar destacado', 'santiago-wine-club' ),
		'not_found'             => __( 'Nada encontrado', 'santiago-wine-club' ),
		'not_found_in_trash'    => __( 'Nada encontrado en la papelera', 'santiago-wine-club' ),
		'featured_image'        => __( 'Imagen destacada', 'santiago-wine-club' ),
		'set_featured_image'    => __( 'Seleccionar imagen para el destacado', 'santiago-wine-club' ),
		'remove_featured_image' => __( 'Remover imagen', 'santiago-wine-club' ),
		'use_featured_image'    => __( 'Usar esta imagen para el destacado', 'santiago-wine-club' ),
		'insert_into_item'      => __( 'Insertar en el destacado', 'santiago-wine-club' ),
		'uploaded_to_this_item' => __( 'Subido a este destacado', 'santiago-wine-club' ),
		'items_list'            => __( 'Lista de destacados', 'santiago-wine-club' ),
		'items_list_navigation' => __( 'Navegar por lista de destacados', 'santiago-wine-club' ),
		'filter_items_list'     => __( 'Filtrar lista de destacados', 'santiago-wine-club' ),
	);
	$args = array(
		'label'                 => __( 'Destacado', 'santiago-wine-club' ),
		'description'           => __( 'Destacados', 'santiago-wine-club' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-visibility',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'destacado', $args );

}
add_action( 'init', 'destacado_post_type', 0 );

function alianza_post_type() {

	$labels = array(
		'name'                  => _x( 'Alianzas', 'Post Type General Name', 'santiago-wine-club' ),
		'singular_name'         => _x( 'Alianza', 'Post Type Singular Name', 'santiago-wine-club' ),
		'menu_name'             => __( 'Alianzas', 'santiago-wine-club' ),
		'name_admin_bar'        => __( 'Alianza', 'santiago-wine-club' ),
		'archives'              => __( 'Archivos de alianzas', 'santiago-wine-club' ),
		'parent_item_colon'     => __( 'Alianza principal:', 'santiago-wine-club' ),
		'all_items'             => __( 'Todas las alianzas', 'santiago-wine-club' ),
		'add_new_item'          => __( 'Añadir alianza', 'santiago-wine-club' ),
		'add_new'               => __( 'Añadir nueva', 'santiago-wine-club' ),
		'new_item'              => __( 'Nueva alianza', 'santiago-wine-club' ),
		'edit_item'             => __( 'Editar alianza', 'santiago-wine-club' ),
		'update_item'           => __( 'Actualizar alianza', 'santiago-wine-club' ),
		'view_item'             => __( 'Ver alianza', 'santiago-wine-club' ),
		'search_items'          => __( 'Buscar alianza', 'santiago-wine-club' ),
		'not_found'             => __( 'Nada encontrado', 'santiago-wine-club' ),
		'not_found_in_trash'    => __( 'Nada encontrado en la papelera', 'santiago-wine-club' ),
		'featured_image'        => __( 'Imagen destacada', 'santiago-wine-club' ),
		'set_featured_image'    => __( 'Seleccionar imagen para la alianza', 'santiago-wine-club' ),
		'remove_featured_image' => __( 'Remover imagen', 'santiago-wine-club' ),
		'use_featured_image'    => __( 'Usar esta imagen para la alianza', 'santiago-wine-club' ),
		'insert_into_item'      => __( 'Insertar en la alianza', 'santiago-wine-club' ),
		'uploaded_to_this_item' => __( 'Subido a esta alianza', 'santiago-wine-club' ),
		'items_list'            => __( 'Lista de alianzas', 'santiago-wine-club' ),
		'items_list_navigation' => __( 'Navegar por lista de alianzas', 'santiago-wine-club' ),
		'filter_items_list'     => __( 'Filtrar lista de alianzas', 'santiago-wine-club' ),
	);
	$args = array(
		'label'                 => __( 'Alianza', 'santiago-wine-club' ),
		'description'           => __( 'Alianzas', 'santiago-wine-club' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-external',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'alianza', $args );

}
add_action( 'init', 'alianza_post_type', 0 );

function agenda_post_type() {

	$labels = array(
		'name'                  => _x( 'Agendas', 'Post Type General Name', 'santiago-wine-club' ),
		'singular_name'         => _x( 'Agenda', 'Post Type Singular Name', 'santiago-wine-club' ),
		'menu_name'             => __( 'Agendas', 'santiago-wine-club' ),
		'name_admin_bar'        => __( 'Agenda', 'santiago-wine-club' ),
		'archives'              => __( 'Archivos de agendas', 'santiago-wine-club' ),
		'parent_item_colon'     => __( 'Agenda principal:', 'santiago-wine-club' ),
		'all_items'             => __( 'Todas las agendas', 'santiago-wine-club' ),
		'add_new_item'          => __( 'Añadir agenda', 'santiago-wine-club' ),
		'add_new'               => __( 'Añadir nueva', 'santiago-wine-club' ),
		'new_item'              => __( 'Nueva agenda', 'santiago-wine-club' ),
		'edit_item'             => __( 'Editar agenda', 'santiago-wine-club' ),
		'update_item'           => __( 'Actualizar agenda', 'santiago-wine-club' ),
		'view_item'             => __( 'Ver agenda', 'santiago-wine-club' ),
		'search_items'          => __( 'Buscar agenda', 'santiago-wine-club' ),
		'not_found'             => __( 'Nada encontrado', 'santiago-wine-club' ),
		'not_found_in_trash'    => __( 'Nada encontrado en la papelera', 'santiago-wine-club' ),
		'featured_image'        => __( 'Imagen destacada', 'santiago-wine-club' ),
		'set_featured_image'    => __( 'Seleccionar imagen para la agenda', 'santiago-wine-club' ),
		'remove_featured_image' => __( 'Remover imagen', 'santiago-wine-club' ),
		'use_featured_image'    => __( 'Usar esta imagen para la agenda', 'santiago-wine-club' ),
		'insert_into_item'      => __( 'Insertar en la agenda', 'santiago-wine-club' ),
		'uploaded_to_this_item' => __( 'Subido a esta agenda', 'santiago-wine-club' ),
		'items_list'            => __( 'Lista de agendas', 'santiago-wine-club' ),
		'items_list_navigation' => __( 'Navegar por lista de agendas', 'santiago-wine-club' ),
		'filter_items_list'     => __( 'Filtrar lista de agendas', 'santiago-wine-club' ),
	);
	$args = array(
		'label'                 => __( 'Agenda', 'santiago-wine-club' ),
		'description'           => __( 'Agendas', 'santiago-wine-club' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'agenda', $args );

}
add_action( 'init', 'agenda_post_type', 0 );

function vinatero_post_type() {

	$labels = array(
		'name'                  => _x( 'Viñateros', 'Post Type General Name', 'santiago-wine-club' ),
		'singular_name'         => _x( 'Viñatero', 'Post Type Singular Name', 'santiago-wine-club' ),
		'menu_name'             => __( 'Viñateros', 'santiago-wine-club' ),
		'name_admin_bar'        => __( 'Viñatero', 'santiago-wine-club' ),
		'archives'              => __( 'Archivos de viñateros', 'santiago-wine-club' ),
		'parent_item_colon'     => __( 'Viñatero principal:', 'santiago-wine-club' ),
		'all_items'             => __( 'Todos las viñateros', 'santiago-wine-club' ),
		'add_new_item'          => __( 'Añadir viñatero', 'santiago-wine-club' ),
		'add_new'               => __( 'Añadir nuevo', 'santiago-wine-club' ),
		'new_item'              => __( 'Nuevo viñatero', 'santiago-wine-club' ),
		'edit_item'             => __( 'Editar viñatero', 'santiago-wine-club' ),
		'update_item'           => __( 'Actualizar viñatero', 'santiago-wine-club' ),
		'view_item'             => __( 'Ver viñatero', 'santiago-wine-club' ),
		'search_items'          => __( 'Buscar viñatero', 'santiago-wine-club' ),
		'not_found'             => __( 'Nada encontrado', 'santiago-wine-club' ),
		'not_found_in_trash'    => __( 'Nada encontrado en la papelera', 'santiago-wine-club' ),
		'featured_image'        => __( 'Imagen destacada', 'santiago-wine-club' ),
		'set_featured_image'    => __( 'Seleccionar imagen para el viñatero', 'santiago-wine-club' ),
		'remove_featured_image' => __( 'Remover imagen', 'santiago-wine-club' ),
		'use_featured_image'    => __( 'Usar esta imagen para el viñatero', 'santiago-wine-club' ),
		'insert_into_item'      => __( 'Insertar en el viñatero', 'santiago-wine-club' ),
		'uploaded_to_this_item' => __( 'Subido a este viñatero', 'santiago-wine-club' ),
		'items_list'            => __( 'Lista de viñateros', 'santiago-wine-club' ),
		'items_list_navigation' => __( 'Navegar por lista de viñateros', 'santiago-wine-club' ),
		'filter_items_list'     => __( 'Filtrar lista de viñateros', 'santiago-wine-club' ),
	);
	$args = array(
		'label'                 => __( 'Viñatero', 'santiago-wine-club' ),
		'description'           => __( 'Viñateros', 'santiago-wine-club' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'vinatero', $args );

}
add_action( 'init', 'vinatero_post_type', 0 );