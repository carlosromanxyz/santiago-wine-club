<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Santiago_Wine_Club
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function santiago_wine_club_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'santiago_wine_club_body_classes' );

// WooCommerce Support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// No breadcrumbs in Woocommerce
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);

// Añadimos un wrapper al inicio y final del loop
add_action( 'woocommerce_before_main_content','woocommerce_bootstrap_start');
function woocommerce_bootstrap_start () {
	echo '<div class="col-xs-12 col-lg-8">';
}
add_action( 'woocommerce_after_main_content','woocommerce_bootstrap_end');
function woocommerce_bootstrap_end () {
	echo '</div>';
}

// Change number or products per row to 3
add_filter( 'loop_shop_columns', 'wc_loop_shop_columns', 1, 10 );
/*
* Return a new number of maximum columns for shop archives
* @param int Original value
* @return int New number of columns
*/
function wc_loop_shop_columns( $number_columns ) {
	return 3;
}

// Display 24 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );