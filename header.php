<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Santiago_Wine_Club
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site container">

	<header id="masthead" class="site-header col-xs-12" role="banner">
		<div class="site-branding">
			<div id="logo" class="col-xs-12 col-lg-3">
				<a href="http://santiagowineclub.cl/"><img src="<?php bloginfo('template_directory') ?>/images/logo.png"></a>
			</div>
			<div id="leyenda" class="col-lg-9 hidden-xs">
				<p><?php _e('Especialista en Chile en Vinos de Terroir, Autor y viñas independientes', 'santiago-wine-club'); ?></p>
			</div>
			<div class="clear"></div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</button>
			<?php
				/**
				* Displays a navigation menu
				* @param array $args Arguments
				*/
				$args = array(
					'theme_location' => 'primary',
					'menu' => '',
					'container' => 'div',
					'container_class' => 'menu-{menu-slug}-container',
					'container_id' => '',
					'menu_class' => 'menu',
					'menu_id' => '',
					'echo' => true,
					'fallback_cb' => 'wp_page_menu',
					'before' => '',
					'after' => '',
					'link_before' => '',
					'link_after' => '',
					'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
					'depth' => 0,
					'walker' => ''
				);
			
				wp_nav_menu( $args ); ?>
		</nav><!-- #site-navigation -->
		<ul id="woocommerce-admin-menu">
			<?php if(is_user_logged_in()) : ?>
				<li>
					<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Mi cuenta', 'santiago-wine-club'); ?>">
						<i class="fa fa-user" aria-hidden="true"></i>
						<span><?php _e('Perfil', 'santiago-wine-club'); ?></span>
					</a>
				</li>
				<li>
					<a href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e('Carro', 'santiago-wine-club'); ?>">
						<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						<span><?php _e('Carro', 'santiago-wine-club'); ?></span>
					</a>
				</li>
				<li>
					<a href="<?php echo wp_logout_url( home_url() ); ?>" title="<?php _e('Cerrar sesión', 'santiago-wine-club'); ?>">
						<i class="fa fa-times-circle" aria-hidden="true"></i>
						<span><?php _e('Salir', 'santiago-wine-club'); ?></span>
					</a>
				</li>
			<?php else : ?>
				<li>
					<a href="<?php echo esc_url( home_url( '/mi-cuenta' ) ); ?>" title="<?php _e('Iniciar sesión', 'santiago-wine-club'); ?>">
						<i class="fa fa-sign-in" aria-hidden="true"></i>
						<span><?php _e('Iniciar sesión | Registrarse', 'santiago-wine-club'); ?></span>	
					</a>
				</li>
			<?php endif; ?>
		</ul>
	</header><!-- #masthead -->

	<div class="clear"></div>

	<div id="content" class="site-content">
